terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }

  backend "http" {
  }
}

locals {
  groupe1 = {
    cferrand = {
      principal_id = "07907939-33fe-45d2-a621-347b863b8948"
    }
    tquach = {
      principal_id = "784d201f-dce3-4920-a541-780a356b9e68"
    }
    wvalerio = {
      principal_id = "09842680-21ca-4310-a3bb-7c07afb58a63"
    }
    kzeghmati = {
      principal_id = "c2e0a49e-b437-4dcb-8bfd-30fc99651b76"
    }
  }

  groupe2 = {
    abroumi = {
      principal_id = "53dc4c9c-3f94-417f-a7ad-6fc4642c5b0d"
    }
    mboulli = {
      principal_id = "abf6322e-4427-4cb9-b2bb-66b02369f24f"
    }
    mdebot = {
      principal_id = "7577f92e-87a9-4479-92f3-10e030665e98"
    }
    cbaumont = {
      principal_id = "6657a8ea-e868-496c-9180-836bc0f03cb8"
    }
  }

  groupe3 = {
    gcaferra = {
      principal_id = "beee6421-90a1-4daf-8e72-6db181182643"
    }
    rzein = {
      principal_id = "1720026a-eef4-4cee-962a-cb4e12691471"
    }
    dardiles = {
      principal_id = "a3443234-555e-4c4b-ad6a-6f47468bf7d1"
    }
  }

  groupe4 = {
    theadrapson = {
      principal_id = "ef93c8c2-6d22-4216-ac42-145b4988396f"
    }
    nboukachab = {
      principal_id = "6b300ba5-e864-4cac-b380-6fc391aed854"
    }
    avasilache = {
      principal_id = "60d818b5-0a6d-4ef2-aa9c-1b7db0745d5a"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_subscription" "subscription" {
}

resource "azurerm_resource_group" "p3_rg" {
  name     = "p7-projet3"
  location = "France Central"
}

#
# Groupe 1 - Decision
#

resource "azurerm_cognitive_account" "anomaly_detector" {
  name                = "p7-projet3-anomaly-detector"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "AnomalyDetector"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "anomaly_detector_role_assignment" {
  for_each = local.groupe1

  scope                = azurerm_cognitive_account.anomaly_detector.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "content_moderator" {
  name                = "p7-projet3-content-moderator"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "ContentModerator"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "content_moderator_role_assignment" {
  for_each = local.groupe1

  scope                = azurerm_cognitive_account.content_moderator.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "personalizer" {
  name                = "p7-projet3-personalizer"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "Personalizer"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "personalizer_role_assignment" {
  for_each = local.groupe1

  scope                = azurerm_cognitive_account.personalizer.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

#
# Groupe 2 - Vision
#

resource "azurerm_cognitive_account" "computer_vision" {
  name                = "p7-projet3-computer-vision"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "ComputerVision"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "computer_vision_role_assignment" {
  for_each = local.groupe2

  scope                = azurerm_cognitive_account.computer_vision.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "custom_vision_training" {
  name                = "p7-projet3-custom-vision-training"
  location            = "West Europe"
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "CustomVision.Training"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "custom_vision_training_role_assignment" {
  for_each = local.groupe2

  scope                = azurerm_cognitive_account.custom_vision_training.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "custom_vision_prediction" {
  name                = "p7-projet3-custom-vision-prediction"
  location            = "West Europe"
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "CustomVision.Prediction"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "custom_vision_prediction_role_assignment" {
  for_each = local.groupe2

  scope                = azurerm_cognitive_account.custom_vision_prediction.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "face" {
  name                = "p7-projet3-face"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "Face"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "face_role_assignment" {
  for_each = local.groupe2

  scope                = azurerm_cognitive_account.face.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

#
# Groupe 3 - Language
#

resource "azurerm_storage_account" "storage_account" {
  name                     = "p7projet3storage"
  resource_group_name      = azurerm_resource_group.p3_rg.name
  location                 = azurerm_resource_group.p3_rg.location
  account_kind             = "Storage"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_cognitive_account" "text_analytics" {
  name                = "p7-projet3-text-analytics"
  location            = "West Europe"
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "TextAnalytics"
  sku_name            = "S"
}

resource "azurerm_role_assignment" "text_analytics_role_assignment" {
  for_each = local.groupe3

  scope                = azurerm_cognitive_account.text_analytics.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "text_translation" {
  name                = "p7-projet3-text-translation"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "TextTranslation"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "text_translation_role_assignment" {
  for_each = local.groupe3

  scope                = azurerm_cognitive_account.text_translation.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

# resource "azurerm_cognitive_account" "qna_maker" {
#   name                = "p7-projet3-qna-maker"
#   location            = azurerm_resource_group.p3_rg.location
#   resource_group_name = azurerm_resource_group.p3_rg.name
#   kind                = "QnAMaker"
#   sku_name            = "F0"
# }

# resource "azurerm_role_assignment" "qna_maker_role_assignment" {
#   for_each = local.groupe3
# 
#   scope                = azurerm_cognitive_account.qna_maker.id
#   role_definition_name = "Contributor"
#   principal_id         = each.value.principal_id
# }

resource "azurerm_cognitive_account" "luis" {
  name                = "p7-projet3-luis"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "LUIS"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "luis_role_assignment" {
  for_each = local.groupe3

  scope                = azurerm_cognitive_account.luis.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_cognitive_account" "luis_authoring" {
  name                = "p7-projet3-luis-authoring"
  location            = "West Europe"
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "LUIS.Authoring"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "luis_authoring_role_assignment" {
  for_each = local.groupe3

  scope                = azurerm_cognitive_account.luis_authoring.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

#
# Groupe 4 - Speech
#

resource "azurerm_cognitive_account" "speech_services" {
  name                = "p7-projet3-speech-services"
  location            = azurerm_resource_group.p3_rg.location
  resource_group_name = azurerm_resource_group.p3_rg.name
  kind                = "SpeechServices"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "speech_services_role_assignment" {
  for_each = local.groupe4

  scope                = azurerm_cognitive_account.speech_services.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}
